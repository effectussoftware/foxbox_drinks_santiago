import { combineReducers } from 'redux';
import cocktailsReducer from '../Containers/CocktailsList/duck/reducer';

export default combineReducers({
  cocktails: cocktailsReducer,
});
