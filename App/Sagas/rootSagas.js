import { all, fork } from "redux-saga/effects";

import cocktailsSagas from "../Containers/CocktailsList/duck/operations";

export default function* rootSaga() {
  yield all([fork(cocktailsSagas)]);
}
