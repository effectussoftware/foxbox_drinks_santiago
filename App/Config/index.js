import DebugConfig from './DebugConfig';

if (__DEV__) {
  // If ReactNative's yellow box warnings are too much, it is possible to turn
  // it off, but the healthier approach is to fix the warnings.  =)
  // eslint-disable-next-line no-console
  console.disableYellowBox = !DebugConfig.yellowBox;
}
