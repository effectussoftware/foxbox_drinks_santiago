import { createSwitchNavigator } from 'react-navigation';

import CocktailsListScreen from '../Containers/CocktailsList';

export const SCREENS = {
  COCKTAILS_LIST: 'CocktailsListScreen',
};

const RootStack = createSwitchNavigator(
  {
    [SCREENS.COCKTAILS_LIST]: CocktailsListScreen,
  },
  {
    initialRouteName: SCREENS.COCKTAILS_LIST,
  },
);

export default RootStack;
