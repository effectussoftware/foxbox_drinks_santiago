import { NavigationActions, StackActions } from "react-navigation";

let navigator;

function setTopLevelNavigator(navigatorRef) {
  navigator = navigatorRef;
}

function navigate(routeName, params) {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
}

const reset = (index, actions, key) => {
  navigator.dispatch(
    StackActions.reset({
      index,
      actions,
      key
    })
  );
};

const popToTop = () => {
  navigator.dispatch(StackActions.popToTop());
};

export default {
  navigate,
  setTopLevelNavigator,
  reset,
  popToTop
};
