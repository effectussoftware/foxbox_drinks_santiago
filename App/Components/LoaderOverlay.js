import { View, ActivityIndicator, StyleSheet } from 'react-native';
import React from 'react';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  activityIndicator: {
    color: 'gray',
  },
});

const LoaderOverlay = () => (
  <View style={styles.container}>
    <ActivityIndicator style={styles.activityIndicator} size="large" />
  </View>
);

export default LoaderOverlay;
