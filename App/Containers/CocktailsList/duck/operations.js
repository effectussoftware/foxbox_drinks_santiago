import { takeEvery, call, put } from 'redux-saga/effects';
import axios from 'axios';

import { Types } from './reducer';

function getList() {
  return axios({
    method: 'get',
    url: 'http://www.thecocktaildb.com/api/json/v1/1/filter.php?g=Cocktail_glass',
  });
}

export function* getListSaga() {
  try {
    const res = yield call(getList);
    yield put({ type: Types.GET_LIST_SUCCESS, response: res.data });
  } catch (error) {
    yield put({ type: Types.GET_LIST_ERROR, error: error.toString() });
  }
}

function getIngredients(idDrink) {
  return axios({
    method: 'get',
    url: `http://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idDrink}`,
  });
}

export function* getIngredientsSaga(action) {
  try {
    const res = yield call(getIngredients, action.idDrink);
    yield put({ type: Types.GET_INGREDIENTS_SUCCESS, response: res.data });
  } catch (error) {
    yield put({ type: Types.GET_INGREDIENTS_ERROR, error: error.toString() });
  }
}

export default function* cocktailsSagas() {
  yield takeEvery(Types.GET_LIST_REQUEST, getListSaga);
  yield takeEvery(Types.GET_INGREDIENTS_REQUEST, getIngredientsSaga);
}
