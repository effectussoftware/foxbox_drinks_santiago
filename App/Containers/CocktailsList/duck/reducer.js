import { createActions } from 'reduxsauce';

export const { Types, Creators } = createActions({
  getListRequest: [],
  getListSuccess: ['response'],
  getListError: ['error'],
  getIngredientsRequest: ['id'],
  getIngredientsSuccess: ['response'],
  getIngredientsError: ['error'],
});

const initialState = {
  cocktailsList: [
    {
      strDrink: '',
      strDrinkThumb: '',
      idDrink: 0,
    },
  ],
};

const ingredientsToSave = (action) => {
  const ingredients = [];
  const keys = Object.keys(action.response.drinks[0]).filter(str => str.includes('Ingredient'));
  const ingredientsKeys = keys.filter(key => action.response.drinks[0][key]);
  ingredientsKeys.forEach(ing => ingredients.push(action.response.drinks[0][ing]));
  return ingredients;
};

const cocktailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_LIST_SUCCESS:
      return {
        ...state,
        cocktailsList: action.response.drinks,
      };
    case Types.GET_INGREDIENTS_SUCCESS:
      return {
        ...state,
        cocktailsList: state.cocktailsList.map(drink => (drink.idDrink === action.response.drinks[0].idDrink
          ? { ...drink, ingredients: ingredientsToSave(action) }
          : drink)),
      };
    default:
      return state;
  }
};

export default cocktailsReducer;
