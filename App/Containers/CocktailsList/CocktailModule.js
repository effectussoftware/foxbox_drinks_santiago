import React from 'react';
import {
  Text, View, Image, ActivityIndicator, FlatList, StyleSheet,
} from 'react-native';
import { objectOf, any } from 'prop-types';
import LoaderOverlay from '../../Components/LoaderOverlay';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 3,
    marginVertical: 5,
    marginHorizontal: 15,
    flexDirection: 'row',
    height: 150,
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 3,
  },
  title: {
    fontSize: 20,
    marginTop: 15,
    paddingLeft: 15,
    paddingRight: 30,
  },
  ingredientsContainer: { flexDirection: 'row', paddingLeft: 15 },
  dot: {
    fontSize: 12,
    marginTop: 4,
    marginRight: 3,
  },
  ingredient: {
    fontSize: 12,
    marginTop: 5,
    paddingRight: 30,
  },
  image: {
    flex: 1,
    borderRadius: 3,
    margin: 12,
  },
  imageContainer: { flex: 3 },
  activity: {
    position: 'absolute',
    left: -60,
    top: 56,
    width: '100%',
  },
});

const renderIngredients = (drink) => {
  // eslint-disable-next-line max-len
  const moreIngredients = drink.ingredients && drink.ingredients.length > 2 && `y ${drink.ingredients.length - 2} ingredientes mas`;
  const ingredientsDisplay = moreIngredients && drink.ingredients.slice(0, 2);
  const ingredientsDisplay1 = moreIngredients ? ingredientsDisplay.concat(moreIngredients) : drink.ingredients;
  return (
    <FlatList
      keyExtractor={item => `${item}`}
      data={ingredientsDisplay1}
      renderItem={({ item, index }) => (
        <View style={styles.ingredientsContainer}>
          {index < 2 && <Text style={styles.dot}>•</Text>}
          <Text style={styles.ingredient}>{item}</Text>
        </View>
      )}
    />
  );
};

const renderImage = drink => (
  <View style={styles.imageContainer}>
    <Image
      style={styles.image}
      loadingIndicatorSource={() => <LoaderOverlay />}
      source={{ uri: drink.strDrinkThumb }}
    />
  </View>
);

const CocktailModule = ({ drink }) => {
  const hasIngredients = !!drink.ingredients;
  return (
    <View style={styles.container}>
      <View style={{ flex: 5 }}>
        <Text style={styles.title}>{drink.strDrink}</Text>
        {hasIngredients ? renderIngredients(drink) : <ActivityIndicator style={styles.activity} />}
      </View>
      {renderImage(drink)}
    </View>
  );
};

CocktailModule.propTypes = {
  drink: objectOf(any).isRequired,
};

export default CocktailModule;
