import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { string } from 'prop-types';

const styles = StyleSheet.create({
  container: { backgroundColor: 'rgb(0, 184, 206)', alignItems: 'center', justifyContent: 'center' },
  title: { color: 'white', fontSize: 15, margin: 10 },
});

const CocktailModule = ({ title }) => (
  <View style={styles.container}>
    <Text style={styles.title}>{title}</Text>
  </View>
);

CocktailModule.propTypes = {
  title: string.isRequired,
};

export default CocktailModule;
