import React, { Component } from 'react';
import {
  FlatList, SafeAreaView, StyleSheet, View, LayoutAnimation,
} from 'react-native';
import { func, arrayOf, any } from 'prop-types';
import { connect } from 'react-redux';
import { Types } from './duck/reducer';

import CocktailModule from './CocktailModule';
import Header from './Header';
import LoaderOverlay from '../../Components/LoaderOverlay';

const styles = StyleSheet.create({
  container: { backgroundColor: 'rgb(0, 184, 206)', flex: 1 },
});
class CocktailsListScreen extends Component {
  componentDidMount() {
    this.props.getDrinksList();
  }

  componentDidUpdate(prevProps) {
    const { cocktailsList, getIngredients } = this.props;
    if (prevProps.cocktailsList.length !== cocktailsList.length) {
      cocktailsList.map(item => getIngredients(item.idDrink));
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }

  render() {
    const { cocktailsList } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <Header title="Random Drinks 0.1" />
        <View style={styles.container}>
          {cocktailsList.length > 1 ? (
            <FlatList
              keyExtractor={item => `${item.strDrink}`}
              data={cocktailsList}
              renderItem={({ item }) => <CocktailModule drink={item} />}
            />
          ) : (
            <LoaderOverlay />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = ({ cocktails: { cocktailsList } }) => ({
  cocktailsList,
});

const mapDispatchToProps = dispatch => ({
  getDrinksList: () => dispatch({ type: Types.GET_LIST_REQUEST }),
  getIngredients: idDrink => dispatch({ type: Types.GET_INGREDIENTS_REQUEST, idDrink }),
});

CocktailsListScreen.propTypes = {
  getIngredients: func.isRequired,
  cocktailsList: arrayOf(any).isRequired,
  getDrinksList: func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CocktailsListScreen);
