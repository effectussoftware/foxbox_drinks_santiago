/* eslint-disable import/no-extraneous-dependencies */
import Reactotron from 'reactotron-react-native';
import '../Config/ReactotronConfig';

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, createStore } from 'redux';

import NavigationService from '../Navigation/NavigationService';
import RootStack from '../Navigation/NavigationStacks';
import rootSagas from '../Sagas/rootSagas';
import reducer from '../Redux';

const sagaMonitor = Reactotron.createSagaMonitor();
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSagas);
class App extends Component {
  render() {
    const AppContainer = createAppContainer(RootStack);
    return (
      <Provider store={store}>
        <AppContainer
          ref={(navigatorRef) => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export default App;
