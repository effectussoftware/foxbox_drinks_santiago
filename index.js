import { AppRegistry } from 'react-native';
import App from './App/Containers/App';

import('./App/Config/ReactotronConfig');

AppRegistry.registerComponent('foxboxDrinksSantiago', () => App);
// eslint-disable-next-line no-console
console.disableYellowBox = true;
